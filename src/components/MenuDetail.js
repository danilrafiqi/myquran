import React, { Component } from 'react';
import { NavBar } from 'antd-mobile';

const navStyle = {
    padding: '20px',
    borderBottom: 'none',
    margin: '10px',
    borderRadius: '7px',
    textAlign: 'right',
    overflow: 'hidden',
    boxShadow: '0 1px 4px rgba(0,0,0,.26)',
    position : 'relative',
}
class Navigasi extends Component {
  render() {
    return (
      <NavBar
      style={navStyle}
      mode="light"
      leftContent={this.props.turun}
      rightContent={`${this.props.jumlah} Ayat`}
      >{this.props.surah}<br/>{this.props.surahEng}</NavBar>
    );
  }
}

export default Navigasi