import React from 'react';
import { Tabs } from 'antd-mobile';
import { StickyContainer, Sticky } from 'react-sticky';

import ListSurah from '../containers/surah/ListSurah';
import ListJuz from '../containers/juz/ListJuz';
import AsmaulHusna from '../containers/tambahan/AsmaulHusna';

function renderTabBar(props) {
  return (
    <Sticky>
      {({ style }) => (
        <div style={{ ...style, zIndex: 1 }}>
          <Tabs.DefaultTabBar {...props} />
        </div>
      )}
    </Sticky>
  );
}
const tabs = [{ title: 'Surah' }, { title: 'Juz' }, { title: 'Asmaul Husna' }];

const IniTab = () => (
  <div>
    <StickyContainer>
      <Tabs tabs={tabs} initalPage={'t2'} renderTabBar={renderTabBar}>
        <div
          style={{
            display: '',
            alignItems: 'center',
            justifyContent: 'center',
            height: '100%',
            backgroundColor: '#fff'
          }}>
          <ListSurah />
        </div>

        <div
          style={{
            display: '',
            alignItems: 'center',
            justifyContent: 'center',
            height: '100%',
            backgroundColor: '#fff'
          }}>
          <ListJuz />
        </div>

        <div
          style={{
            display: '',
            alignItems: 'center',
            justifyContent: 'center',
            height: '100%',
            backgroundColor: '#fff'
          }}>
          <AsmaulHusna />
        </div>
      </Tabs>
    </StickyContainer>
  </div>
);

export default IniTab;
