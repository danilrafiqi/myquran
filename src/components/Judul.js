import React, { Component } from 'react';

const judulStyle = {
  padding: '20px',
  margin: '12px',
  borderRadius: '7px',
  textAlign: 'center',
  overflow: 'hidden',
  boxShadow: '0 1px 4px rgba(0,0,0,.26)',
  color: '#5d5d5d'
};

class Judul extends Component {
  render() {
    return <div style={judulStyle}>{this.props.judul}</div>;
  }
}

export default Judul;
