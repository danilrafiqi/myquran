import React, { Component } from 'react';

import Navigasi from './Navigasi';
import IniTab from './IniTab';

class App extends Component {
  render() {
    return (
      <React.Fragment>
        <Navigasi />
        <IniTab />
      </React.Fragment>
    );
  }
}

export default App;
