import React, { Component } from 'react';
import { Popover, NavBar, Icon } from 'antd-mobile';
import { Link } from 'react-router-dom';

const Item = Popover.Item;

class Navigasi extends Component {
  state = {
    visible: false,
    selected: ''
  };
  onSelect = opt => {
    // console.log(opt.props.value);
    this.setState({
      visible: false,
      selected: opt.props.value
    });
  };
  handleVisibleChange = visible => {
    this.setState({
      visible
    });
  };

  render() {
    return (
      <NavBar
        mode="dark"
        icon={
          <Link to="/" style={{ color: 'white', display: 'flex' }}>
            <Icon type="left" />
          </Link>
        }
        rightContent={[
          <Icon key="0" type="search" style={{ marginRight: '16px' }} />,
          <Popover
            key="1"
            mask
            overlayClassName="fortest"
            overlayStyle={{ color: 'currentColor' }}
            visible={this.state.visible}
            overlay={[
              <Item key="1">
                <Link to="/" style={{ whiteSpace: 'nowrap' }}>
                  Home
                </Link>
              </Item>,
              <Item key="2">
                <Link to="/petunjuk" style={{ whiteSpace: 'nowrap' }}>
                  Petunjuk
                </Link>
              </Item>,
              <Item key="3">
                <Link to="/tentang" style={{ whiteSpace: 'nowrap' }}>
                  Tentang
                </Link>
              </Item>
            ]}
            align={{
              overflow: { adjustY: 0, adjustX: 0 },
              offset: [-10, 0]
            }}
            onVisibleChange={this.handleVisibleChange}
            onSelect={this.onSelect}>
            <div
              style={{
                height: '100%',
                padding: '0 15px',
                marginRight: '-15px',
                display: 'flex',
                alignItems: 'center'
              }}>
              <Icon type="ellipsis" />
            </div>
          </Popover>
        ]}>
        MyQuran
      </NavBar>
    );
  }
}

export default Navigasi;
