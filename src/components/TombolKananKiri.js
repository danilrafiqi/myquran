import React, { Component } from 'react';
import {NavBar, Icon } from 'antd-mobile';
import {Link} from 'react-router-dom'

class Navigasi extends Component {
  state = {
    visible: false,
    selected: '',
  };
  onSelect = (opt) => {
    // console.log(opt.props.value);
    this.setState({
      visible: false,
      selected: opt.props.value,
    });
  };
  handleVisibleChange = (visible) => {
    this.setState({
      visible,
    });
  };

  render() {

    return (
      <NavBar
      style={{
        zIndex: '1',
        position: 'fixed',
        bottom: '0',
        left: '0',
        right: '0'
      }}
      mode="light"
      icon={<Link to={`/`} style={{ color: '#108ee9', display: 'flex' }}><Icon type="left"/></Link>}
      onLeftClick={() => console.log('onLeftClickss')}

      rightContent={[
        <Link key="0" to='/' style={{ color: '#108ee9', display: 'flex',marginRight: '16px' }}><Icon type="right"/></Link>
      ]}
      >MyQuran</NavBar>
    );
  }
}

export default Navigasi