import React, { Component } from 'react';
import Axios from 'axios'
import Navigasi from '../../components/Navigasi'
// import KananKiri from '../../components/TombolKananKiri'
import MenuDetail from '../../components/MenuDetail'

const ulStyle = {
	paddingLeft: '0',
    margin: '0 10px'
}

const liStyle = {
    padding: '20px',
    borderBottom: 'none',
    margin: '0 0 12px',
    borderRadius: '7px',
    textAlign: 'right',
    overflow: 'hidden',
    boxShadow: '0 1px 4px rgba(0,0,0,.26)',
    position : 'relative',
    color: '#5d5d5d',
    background: 'white'
}

const spanAyat = {
	position: 'absolute',
    left: '0',
    marginLeft: '10px'
}
export default class DetailSurah extends Component {

	state = {
		surah:[],
		ayat:[]
	}

	all = () => {
		console.log('prpsss', this.props)
		Axios
		  .get(`https://api.alquran.cloud/surah/${this.props.match.params.ayat}/editions/ar.alafasy,id.indonesian`)
		  .then(res =>{
		  	console.log('iki datane', res.data.data)
		    this.setState({
		      surah:res.data.data[0],
		      ayat:res.data.data[0].ayahs,
		      ayatIndo:res.data.data[1].ayahs,
		    })
		  })
	}

	componentDidMount(){
		this.all()
	}

	render() {
		return (
			<React.Fragment>
		        <Navigasi/>
		        <MenuDetail
		        	surah={this.state.surah.name}
		        	surahEng={this.state.surah.englishName}
		        	jumlah={this.state.surah.numberOfAyahs}
		        	turun={this.state.surah.revelationType}
		        />
				<ul style={ulStyle}>
			      {
			        this.state.ayat.map((ayats, index) => {
			        	return (
							<li style={liStyle} key={index} >
								<span style={spanAyat}>{ayats.numberInSurah}</span>
								<div>{ayats.text}</div>
								<br/>
								<div>{this.state.ayatIndo[index].text}</div>
								<br/>
							    <audio style={{width:'100%'}}src={ayats.audio} controls></audio>
							</li>
			        	)
			        })
			      }

				</ul>
			</React.Fragment>
		);
	}
}
