import React from 'react';
import { List } from 'antd-mobile';
import { Link } from 'react-router-dom'
import Axios from 'axios'

const Item = List.Item;
const Brief = Item.Brief;

export default class ListQuran extends React.Component {
  state = {
    disabled: false,
    data:[],
    asmaulhusna:[]
  }

  all = () => {
    Axios
      .get('https://api.alquran.cloud/surah')
      .then(res =>{
        this.setState({
          data:res.data.data,
        })
      })
  }

  componentDidMount(){
    this.all()
  }

  render() {
    return (<div>
      <List className="my-list">
      {
        this.state.data.map((datas, index) => {
          return (
            <Link key={index} to={`/quran/surah/${datas.number}`}>
              <Item
                arrow="horizontal"
                multipleLine
                onClick={() => {}}
                platform="android"
              >
              {datas.englishName}
              <Brief>{datas.numberOfAyahs} Ayat</Brief>
              </Item>
          </Link>
          )
        })
      }
      </List>
    </div>);
  }
}
