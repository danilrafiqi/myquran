import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import Layout from '../components/Layout';
import Home from './welcome/Home';
import Surah from './surah/DetailSurahNew';
import Juz from './juz/DetailJuz';
import About from '../containers/about/About';
import Petunjuk from '../containers/Petunjuk';
import Jadwal from '../containers/tambahan/JadwalUse';

export default class MainRouter extends Component {
  render() {
    return (
      <Switch>
        <Route exact path="/" component={Home} />
        <Route exact path="/tentang" component={About} />
        <Route exact path="/adzan" component={Jadwal} />
        <Route exact path="/petunjuk" component={Petunjuk} />
        <Route exact path="/quran" component={Layout} />
        <Route path="/quran/surah/:ayat" component={Surah} />
        <Route path="/quran/juz/:number" component={Juz} />
      </Switch>
    );
  }
}
