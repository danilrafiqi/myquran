import React from 'react';
import { List } from 'antd-mobile';
import {Link} from 'react-router-dom'

const Item = List.Item;

export default class ListQuran extends React.Component {
  state = {
    disabled: false,
    data:[1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 11,12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24 ,25 ,26 ,27 ,28 ,29 ,30]
  }

  render() {
    return (<div>
      <List className="my-list">
      {
        this.state.data.map((datas, index) => {
          return (
            <Link key={index} to={`quran/juz/${datas}`}>
              <Item
                arrow="horizontal"
                multipleLine
                onClick={() => {}}
                platform="android"
              >
              Juz {datas}
              </Item>
          </Link>
          )
        })
      }
      </List>
    </div>);
  }
}
