import React, { Component } from 'react';
import Axios from 'axios'
import Navigasi from '../../components/Navigasi'

import { NavBar } from 'antd-mobile';

const ulStyle = {
	paddingLeft: '0',
    margin: '0 10px'
}

const liStyle = {
    padding: '20px',
    borderBottom: 'none',
    margin: '0 0 12px',
    borderRadius: '7px',
    textAlign: 'right',
    overflow: 'hidden',
    boxShadow: '0 1px 4px rgba(0,0,0,.26)',
    position : 'relative',
    color: '#5d5d5d'
}

const spanAyat = {
	position: 'absolute',
    left: '0',
    marginLeft: '10px'
}
export default class DetailSurah extends Component {

	state = {
		surah:[],
		ayat:[]
	}

	all = () => {
		console.log('prpsss', this.props)
		Axios
		  .get(`https://api.alquran.cloud/juz/${this.props.match.params.number}`)
		  .then(res =>{
		  	console.log('iki datane', res.data.data)
		    this.setState({
		      surah:res.data.data,
		      ayat:res.data.data.ayahs,
		    })
		  })
	}

	componentDidMount(){
		this.all()
	}

	render() {
		return (
			<React.Fragment>
		        <Navigasi/>

				<NavBar
				mode="light"
				leftContent=''
				rightContent=''
				>Juz {this.props.match.params.number}</NavBar>

				<ul style={ulStyle}>
			      {
			        this.state.ayat.map((ayats, index) => {
			        	return (
							<li style={liStyle} key={index} ><span style={spanAyat}>{ayats.numberInSurah}</span>{ayats.text}</li>
			        	)
			        })
			      }

				</ul>
			</React.Fragment>
		);
	}
}
