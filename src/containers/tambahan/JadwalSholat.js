import React, { Component } from 'react';
import { Accordion } from 'antd-mobile';
import Axios from 'axios';

class JadwalSholat extends Component {
  state = {
    jadwal: []
  };

  jadwals = () => {
    Axios.get(
      `https://api.aladhan.com/v1/calendarByCity?city=Lampung&country=Indonesia&method=2&month=11&year=2018`
    ).then(res => {
      this.setState({
        jadwal: res.data.data
      });
    });
  };
  componentDidMount() {
    this.jadwals();
  }

  render() {
    return (
      <div style={{ marginTop: 10, marginBottom: 10 }}>
        <Accordion
          defaultActiveKey="0"
          className="my-accordion"
          onChange={this.onChange}>
          {this.state.jadwal.map((jdwl, index) => {
            return (
              <Accordion.Panel
                key={index}
                header={jdwl.date.readable}
                className="pad">
                <ul>
                  <li>Subuh {jdwl.timings.Fajr}</li>
                  <li>Dzuhur {jdwl.timings.Dhuhr}</li>
                  <li>Ashar {jdwl.timings.Asr}</li>
                  <li>Maghrib {jdwl.timings.Maghrib}</li>
                  <li>Isya {jdwl.timings.Isha}</li>
                </ul>
              </Accordion.Panel>
            );
          })}
        </Accordion>
      </div>
    );
  }
}

export default JadwalSholat;
