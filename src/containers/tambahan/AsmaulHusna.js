import React, { Component } from 'react';
import { Grid } from 'antd-mobile';
import Axios from 'axios'

class AsmaulHusna extends Component {
  state = {
    asmaulhusna:[]
  }

  asmaulhusna = () => {
    Axios
      .get('https://api.aladhan.com/asmaAlHusna')
      .then(res =>{
        this.setState({
          asmaulhusna:res.data.data.map((_val, i) => ({
            arab: `${_val.name}`,
            text: `${_val.transliteration}`,
          }))
        })
      })
  }
  componentDidMount(){
    this.asmaulhusna()
  }

  render() {
    return (
      <Grid data={this.state.asmaulhusna}
        columnNum={3}
        renderItem={dataItem => (
          <div style={{ padding: '25% 0' }}>
            <div>{dataItem.arab}</div>
            <div style={{ color: '#888', fontSize: '14px', marginTop: '12px' }}>
              <span>{dataItem.text}</span>
            </div>
          </div>
        )}
      />
    );
  }
}

export default AsmaulHusna