import React, { Component } from 'react';
import Navigasi from '../../components/Navigasi';
import JadwalSholat from './JadwalSholat';

class JadwalUse extends Component {
  render() {
    return (
      <React.Fragment>
        <Navigasi />
        <JadwalSholat />
      </React.Fragment>
    );
  }
}

export default JadwalUse;
