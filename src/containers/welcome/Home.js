import React, { Component } from 'react';
// import MenuHome from '../../components/MenuHome'
import { Link } from 'react-router-dom';

import logo from '../../logo-lg-w.png';

const menu = [
  {
    judul: 'AlQuran',
    link: '/quran'
  },
  {
    judul: 'Jadwal Sholat',
    link: '/adzan'
  },
  {
    judul: 'Petunjuk',
    link: '/petunjuk'
  },
  {
    judul: 'Tentang',
    link: '/tentang'
  }
];

export default class Home extends Component {
  render() {
    return (
      <div className="menu-wrapper">
        <div id="menu" className="open-menu" />
        <ul className="menus">
          <li className="menu-item item-show">
            <img
              alt="Logo"
              src={logo}
              style={{ width: '70%', margin: '.1rem auto', display: 'flex' }}
            />
          </li>
          {menu.map((menus, index) => {
            return (
              <li key={index} className="menu-item item-show">
                <Link to={menus.link} className="">
                  {menus.judul}
                </Link>
              </li>
            );
          })}
        </ul>
      </div>
    );
  }
}
