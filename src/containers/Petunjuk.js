import React, { Component } from 'react';
import Judul from '../components/Judul';

const divStyle = {
  padding: '20px',
  margin: '12px',
  borderRadius: '7px',
  overflow: 'hidden',
  boxShadow: '0 1px 4px rgba(0,0,0,.26)',
  color: '#5d5d5d'
};

class Petunjuk extends Component {
  render() {
    return (
      <div>
        <Judul judul="Petunjuk" />
        <div style={divStyle}>
          Cara penggunaan aplikasi MyQuran
          <ul>
            <li>Klik pada menu AlQuran </li>
            <li>
              Klik pada tab Surah untuk membaca berdasarkan surah, dan juz untuk
              membaca berdasarkan juz
            </li>
            <li>Klik tombol audio untuk mendengarkan audio</li>
          </ul>
        </div>
      </div>
    );
  }
}

export default Petunjuk;
