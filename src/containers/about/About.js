import React, { Component } from 'react';
import Judul from '../../components/Judul';

const divStyle = {
  padding: '20px',
  margin: '12px',
  borderRadius: '7px',
  overflow: 'hidden',
  boxShadow: '0 1px 4px rgba(0,0,0,.26)',
  color: '#5d5d5d'
};

class About extends Component {
  render() {
    return (
      <div>
        <Judul judul="Tentang" />
        <div style={divStyle}>
          Aplikasi myquran adalah sebuah aplikasi yang digunakan sebagai media
          membaca alqruan dengan cara mengakses web service dari situs online
          berbasis PWA . Aplikasi ini dapat dapat Akses melalui web desktop dan
          juga app mobile.
          <br />
          <br />
          Profil Pengembang
          <ul>
            <li>Dedy Indra Setiawan</li>
            <li>Muhamad Danil Rafiqi</li>
          </ul>
        </div>
      </div>
    );
  }
}

export default About;
